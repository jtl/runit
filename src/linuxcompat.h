//
// Created by James on 4/20/18.
//

#ifndef RUNIT_2_1_2_LINUXCOMPAT_H
#define RUNIT_2_1_2_LINUXCOMPAT_H

#ifdef __linux__

#else

#define CLONE_NEWNET 0
#define CLONE_FILES 1
#define CLONE_NEWPID 2
#define CLONE_NEWIPC 3
#define CLONE_NEWNS 4
#define CLONE_NEWUTS 5
#define CLONE_NEWUSER 6

int setns(int x, int y) { }

int unshare(int x) { }

#define MNT_DETACH 0
int umount2(char *x, int y) { }

#endif

#endif //RUNIT_2_1_2_LINUXCOMPAT_H
