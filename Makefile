PREFIX=/usr/bin

compile:
	./package/compile

copybin:
	cp -a command/* ${PREFIX}

linkinit:	
	rm -f /sbin/init
	ln -s ${PREFIX}/runit-init /sbin/init

install: copybin
	echo "Installed!"

clean:
	rm -rf compile command

dpkg:
	./scripts/dpkg.sh

