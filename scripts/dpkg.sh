#!/bin/bash -xe

PACKAGES="$(cd debian; ls)"
DISTRIBUTION="$(lsb_release -cs)"

for pkg in $PACKAGES; do
    . debian/$pkg
    checkinstall -y --pkgname=$pkg --pkgversion=$version $command
    filename="$pkg_$version-1_amd64.deb"
    dpkg -L $pkg
    curl -f -T $filename -ujtl:"$BINTRAY_KEY" "https://api.bintray.com/content/jtl/server/$pkg/$version/$filename;deb_distribution=$DISTRIBUTION;deb_component=main;deb_architecture=amd64;publish=1;override=1"
    cp $filename "$DISTRIBUTION-$filename"
    curl -f -X POST --user "${BB_AUTH_STRING}" "https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files="@$DISTRIBUTION-$filename"
done
