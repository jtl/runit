#!/bin/bash -xe

echo 'diet -Os gcc -O2 -Wall' >src/conf-cc
echo 'diet -Os gcc -s -Os -pipe' >src/conf-ld
