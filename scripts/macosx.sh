#!/bin/sh -e

case `uname` in
Darwin)
    echo "Updating Makefile for OS X"
    echo 'cc -Xlinker -x' > src/conf-ld
    sed -i '.backup' -e 's/ -static//' src/Makefile
    ;;
*)
    ;;
esac
